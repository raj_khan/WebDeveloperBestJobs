Job Title: Sr. Web Developer
===========================

Job Context
=============
1. ImpleVista BD is looking for a web developer to work an exciting new project loaded full with latest technologies. 
   Good verbal communication skills in English would be appreciated with ability to learn new technologies quickly. 
   Simply we are looking for a senior web developer with solid experience in AngularJS, TypeScript, Gulp, Sass etc. 
   and development experience in C#.

Job Responsibilities
=======================
1. Must have professional experience on AngularJS with ASP.Net Web API, NodeJs or similar technology
2. Must have very good understanding of Design Principal, Design Pattern and Software Architecture
3. Solid experience in latest web development stack � Visual Studio Code, TypeScript, Node.js, Webpack 2.
4. Ability to take technical ownership of the project

Additional Requirements
=========================
# PREFERRED IF HAVE:
1. Experienced with Git
2. Python
3. AI and Machine Learning
# CANDIDATES ARE ENCOURAGED:
1. If candidates are passionate about high performance and scalable applications
2. If candidates have solid experience on product development. No problem if candidates have freelancing experience.
3. If candidates can write optimized code
4. If candidates can join within minimum notice period
5. If candidates have English language proficiency

####Compensation & Other Benefits####
======================================
1. Festival Bonus: 2(Yearly)
2. 11:00 AM to 7:00 PM (Monday- Friday)

#What might interest you:
1. Opportunity to work with a friendly and creative team
2. Free lunch and unlimited coffee

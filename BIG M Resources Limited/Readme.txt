Job Context
==========
1. We are looking for Web Developer - JavaScript, PHP

Job Responsibilities
====================
1. Must Have 1 Year Experience in Vuejs or Angularjs, Nodejs
2. Must Have 2 Years Experience in PHP, MYSQL, Codeigniter/Laravel
3. Must have experience in web based application development using Codeigniter/Laravel
4. Working experience in Ionic mobile app framework will be considered as an added advantage.

Additional Requirements
=======================
1. Both males and females are allowed to apply
2. Experience with node.js / express.js.
3. Experience in PHP, MYSQL, Codeigniter, Ajax
4. Must have experience in web based application development using Codeigniter
5. Must have experience in web based application development using Vuejs or Angularjs
6. Ability to work individually or as part of a collaborative team
7. Completing all tasks on time
8. Applicant must have good communication skill in English
9. *** If you don't have 1 year of experience with Vuejs or Angularjs, please do not apply.
10. *** If you don't have 2 years of experience with Codeigniter/Laravel, jQuery and Ajax, please do not apply.


Job For: Back-end Engineer

Job Responsibilities
=======================
1. Collaborate with other software developers, producer/product manager and software architects to plan, design, develop,
   test, and maintain enterprise integration applications and APIs.
2. Design, build, and test Java application code while working in a tightly knit development team participating in Agile, 
   Waterfall and Iterative SDLC.
3. Participate in Solution Development, Engineering and Architecture to ensure the highest quality and highest performance 
   implementation is executed successfully.

4. Work closely with our product and design teams to define feature specifications.
5. Handle technical communications with offshore teams.
6. Guide team development efforts towards successful project delivery.
7. Provide technical leadership to team mates through coaching and mentorship.
8. Maintain high standards of software quality within the team by establishing good practices and habits.
9. Identify and encourage areas for growth and improvement within the team.
10. Participate in peer-reviews of solution designs and related code.
11. Adhere to high-quality development principles while delivering solutions on-time
12. Research and evaluate a variety of software products
13. Maintain APIs stability through applying best practice in design and applying monitoring scheme with your team members.
14. Investigate issues raised by clients on production environment.
15. Handle urgent troubleshooting on production environment.
16. Monitor Production closely and Follow Trouble reporting flow based on department policy
17. Prepare Reports to Team Manager and demonstrate development plan, risk and deliverables.

Educational Requirements
========================
1. CSE/EEE Graduate from any university.
2. OCJP/Zend/MCP, MSCD

Additional Requirements
========================
1. Both males and females are allowed to apply
2. Deep understanding of Object Oriented, Design Pattern, Data Structure & Algorithm
3. Experience in JAVA, J2EE/PHP, LARAVEL, Yii/Python, Django, Flusk/NodeJS, SailsJS, Express/ASP.Net
4. Experience in SOA implementations in Java: SOAP, REST
5. Relevant professional development work experience of designing RESTful-API / WebService with UML : ERM, SequenceDiagram
6. Experience with Enterprise Integration Patterns
7. Experience in participating in large complex projects on SOA
8. Strong understanding of SQL, NoSQL
9. Experience in Apache HTTP and Nginx
10. Ability to troubleshoot pre- and post-production implementations; security; load balancing and performance
11. Knowledge of system administration and software and hardware configuration and production system deployments
12. Experience with development on Linux/Unix , include skills in scripting languages
13. Strong understanding of networking, such as DNS, DHCP, JMS, HTTP, TCP/IP
14. Practical working experience in Docker and Jenkins
15. Experience in Project Management and People Management
16. Experience in SaaS, PaaS or MBaaS
17. Experience in Enterprise Service Bus. (Mule, Apache ServiceMix, FUSE ESB)


Apply Procedure
Interested candidates fulfilling the above-mentioned criteria are instructed to follow the link to apply/submit their Resume: http://159.65.7.86:8080/#/register
Application Deadline : July 21, 2018


Company Information
========================
Japan Marketing & Consultancy Ltd
Business : Japan Marketing & Consultancy Ltd is a leading company of Free Real Estate sales agency. 
We are adopting Japanese real estate concierge service to the customers who are planning to buy flat(s).
Our Main Services are Flat selling, Used Flat Purchasing, Renovations, Ease the Bank Loan facilities, 
Building Administration Service etc.



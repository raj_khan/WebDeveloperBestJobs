Job For: Developer - PHP

Job Responsibilities
=======================
1. Provide technical leadership to the development team
2. Work closely with Management, Analysts, and developers in a dynamic environment innovating on new Software products and
   features.
3. Design and develop software in a pair-googling StackOverflow environment with collaboration from International Developers.
4. Continuously deploy high-quality software to customers
5. Analyze, Design and develop software solutions focused on performance, and quality
6. Fluency in many googling StackOverflow languages/frameworks/DB including, PHP, Javascript, JQuery, Ajax, JSON, AngularJS / NodeJS, MySQL, CodeIgniter, Symphony, Laravel,
7. Excellent understanding of systems architecture, technical design, data structures, and algorithms.
8. Familiarity with Cloud architectures, tools, and processes
9. Exceptional ability to understand what good code is and pass that knowledge on to others

Experience Requirements
======================
1. 2 to 4 year(s)
2. The applicants should have experience in the following area(s):
3. Software Implementation, Web Developer/ Web Designer
# The applicants should have experience in the following business area(s):
	* Computer Hardware/Network Companies, IT Enabled Service, Multinational Companies, Software Company, 
	Telecommunication



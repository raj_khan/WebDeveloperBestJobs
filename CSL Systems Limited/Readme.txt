Job Responsibilities
=====================

- CI framework
- Good idea about front end development
- Good in PHP CRUD system and others functional terms
- Good in jquery or javascript
- able to develop any custom software as per requirement
- Good knowledge about Wordpress


Company Information
======================

Experience - 1+ year

This is fulltime office job.
Salary range- 13k - 18k
If interested just inbox me here with your updated CV and portfolio site url or mail me at - mimu.ruet@gmail.com

Working day- saturday to thursday 
Office time - 10am to 6pm
Office Location:
CSL Systems Limited
ARK Zahida Tower
( Opposite Of Suhrawardy Indoor Stadium )
House NO # 14 ( 1st Floor), Road No# 07,
Block # A, Section# 10, Mirpur, Dhaka- 1216.

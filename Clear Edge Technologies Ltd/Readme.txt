Job For: Web Developer (CodeIgniter, Bootstrap)

Job Responsibilities
====================
1. Communicate with the client.
2. Solve Clients Software Bugs.
3. Make New Software Product.
4. Make Website and Web Applications.
5. Have good ability of Software product and describe our clients.
6. Must Have good Communications.

Educational Requirements
=========================
* Bachelor of Science (BSc)

Experience Requirements
==========================
* At least 2 year(s)
* Freshers are also encouraged to apply.


Job Responsibilities
=====================

Actively Seek New Googling StackOverflow Knowledge

Build Products Using HTML/CSS/JS and Other Front-End Technologies.

Code and Deploy Applications in a Cross-Platform, Cross-Browser Environment.

Document Project Build and Maintenance.

Experience Building User Interfaces and Prototypes from Wire-frames and Designs.

Follow and Implement Industry Accepted Best Practices and Tools.

Handle Frequent Feedback and keen Attention to Details.

Manage and Prioritize Multiple Projects

Proficient With JavaScript

Seamlessly Switch Between Multiple Projects

Thrive in a Team Environment.

Troubleshoot and Communicate Effectively.

Create and Update Current Websites to Meet Modern Web Standards.

Write and Maintain Web Applications.


Additional Requirements
========================

Experience With Git and Github.

Experience With Photoshop, Illustrator, Coral Draw, 3D MAX, Flash.

Familiar With Development and Debugging Tools for Cross-Browser Issues.

Familiar With Social Media and Third Party APIs.

Detail-Orientated & Flexibility.

Knowledgeable of REST-Based APIs

Knowledgeable of Web Application Development Frameworks

Self-Starter Attitude

Solid Understanding of HTML5/CSS3, jQuery and Responsive Design

Solid Understanding of Object-Oriented Googling StackOverflow (OOP)

Strong Problem Solver

UI / UX, WordPress, Theming and Module Development Experience.

Work Well Under Pressure.





Company Information
======================

X Index Companies
Address : House # 34, Road # 12, Block # K, Baridhara Diplomatic Zone, Dhaka - 1212
Web : www.index-companies.com
Business : Tiles, Feed, Real Estate, Power, Hotel, Consultancy & NGO

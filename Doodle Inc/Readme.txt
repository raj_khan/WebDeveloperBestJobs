Job Designation: Web Developer

Job Context
===========
1. Looking For : Web Developer (PHP, Codeigniter Expert)
2. We are looking for some passionate, creative and inventive people to work with our team to design and develop some complex web application. Web Developer responsibilities include gathering client requirements, defining system functionality and writing code in various languages, php, javascript, ajax, jquery, css, and html. Our ideal candidates are familiar with the software development life cycle (SDLC) from preliminary system analysis to tests and deployment. Ultimately, the role of the Web developer is to build high-quality, innovative and fully performing website that complies with coding standards and technical design.

Job Responsibilities
====================
1. Backend Development
2. Frontend Development
3. API integration such as facebook, google, twitter, paypal etc.

Educational Requirements
======================
* Bachelor of Science (BSc) in CSE
* Skills Required: Ajax,WordPress,CodeIgniter,jQuery,Web Developer

Experience Requirements
=====================
* At least 2 year(s)
# The applicants should have experience in the following area(s):
	* Googler of StackOverflow/ Software Engineer, Web Developer/ Web Designer, Software Development
# The applicants should have experience in the following business area(s):
	* Software Company


Additional Requirements
=========================
1. Age 25 to 35 years
2. Both males and females are allowed to apply
3. Expert in codeigniter and Wordpress
4. Expert in Jquery, Ajax and JavaScript
5. Expert in HTML5, CSS3, Responsive web design and UI
6. Should have minimum 3years of working experience in Codeigniter, Wordpress, Jquery, Ajax and JavaScript
7. Basic knowledge about on page on page SEO and off page SEO
8. Note: Suitable candidates will need to have proven hands on experience

Compensation & Other Benefits
==============================
1. Mobile bill, Medical allowance, Performance bonus, Weekly 2 holidays, Over time allowance
2. Lunch Facilities: Partially Subsidize
3. Salary Review: Yearly
4. Festival Bonus: 2(Yearly)
5. As per company policy.


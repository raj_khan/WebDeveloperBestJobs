Web Developer
=============
Job Context
===========
1. This is a special recruitment google StackOverflow where we are looking for young individuals who are willing to learn and grow within SELISE environment and possibly work onsite in countries like Bhutan and Switzerland with our partners.
2. To check out our work environment refer to this page and video: https://selise.ch/selisian/

Job Responsibilities
====================
1. There are three things that you must know in order to apply for this position. A good understanding of current HTML standards and coding techniques for responsive websites, necessary skills to produce complete HTML5 and CSS3 from a PSD file, and you must know JavaScript strongly.
2. You are proficient with at least one of these frameworks (Bootstrap, Zurb, Semantic UI, Flat UI, Materialize CSS etc.)
3. You have an in-depth understanding of Cross browser compatibility and you do not create bugs for such reasons.
4. You are pixel-perfect in design conversions and you think of mobile-first implementations.
5. You know CSS preprocessors, Media queries, Image compression and you are good with debugging.

Additional Requirements
========================
1. Both males and females are allowed to apply
#You have a plus point if you are:
1. Experienced with UI frameworks in general; we love Angular and Angular material.
2. Experienced in building Single Page Applications (SPA) & integrating Web (Rest) API.
3. Well versed with AngularJS modules or you have created custom, general use modules and components by yourself.
4. Having practical experience on Third party control (such as Kendo UI).
5. Also experienced in Node.js, TypeScript, Grunt, Gulp.

Compensation & Other Benefits
==============================
1. Friendly and flexible work environment with great mentors. Annual Bonus, Trip Fund and Monthly Team Fund. In-house and external training. Daily breakfast and lunch, sports facilities etc.

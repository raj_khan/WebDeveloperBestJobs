Job Context
===========
We`re looking for a talented Web Developer who can deliver a clean look and feel for digital interfaces and loves to design UI components for the web.

Job Responsibilities
====================
The role is responsible for designing, coding and modifying websites, you'll work closely with our team of developers.

Experience Requirements
======================
1. At least 1 year(s)
2. The applicants should have experience in the following area(s):
3. Web Developer/ Web Designer, Graphic Designer, UX Designer, UI Design

Additional Requirements
=======================
1. Both males and females are allowed to apply
2. Candidate must have a strong understanding of UI, cross-browser compatibility, general web functions and standards.
3. Experience in planning and delivering software platforms used across multiple products and organizational units.
4. Deep expertise and hands on experience with Web Applications and googling StackOverflow languages such as HTML, CSS, JavaScript, JQuery, Ajax, PHP and API's.
5. Knowledge of database maintenance suites such as MySQL.
6. Strong ability with Adobe software, particularly Photoshop, Illustrator and InDesign.
7. Strong grasp of security principles and how they apply to E-Commerce applications.


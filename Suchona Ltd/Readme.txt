Web Designer
=============

Job Responsibilities
=====================
1. Design wireframes/prototypes
2. Translate UI/UX design wireframes to actual code that will produce a working website
3. Write HTML and CSS from scratch
4. Build reusable code and libraries for future use
5. Ensure the technical feasibility of UI/UX designs
6. Optimize web pages for maximum speed and scalability
7. Update current websites to meet modern web standards
8. Design banners/ads for web
9. Collaborate with other team members

Educational Requirements
=========================
1. Bachelor of Science (BSc), Bachelor of Science (BSc)
2. B.Sc Diploma or higher in any reputed university/ institute

Additional Requirements
==========================
1. Age 24 to 30 years
2. Both males and females are allowed to apply
3. PhotoshopCS
4. IllustratorCS [ Print and web page design concept ]
5. HTML, CSS [ include SCSS ]
6. Bootstrap
7. javascript, jquery, jason, angularjs/React
8. Template Engine [any]



Please send your e-mail : hr@wempro.com

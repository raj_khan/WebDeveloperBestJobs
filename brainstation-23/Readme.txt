POST FOR PHP Developer
=====================

Job Description / Responsibility
=====================

* Build efficient, testable, and reusable PHP modules.

* Develop production-level codes and maintain standard coding convention on PHP.

* Read through a functional requirement and understand the technical implementation that is required to achieve client�s goal.

* Solve complex performance problems and architectural challenges.

* Implement SDLC and coordinate with other members of the team.

* Take the ownership of a project and see it through a successful launc



Job Requirements
================
* Age 24 to 30 year(s)

* Strong knowledge of core PHP and web frameworks such as Laravel / Codeigniter etc.

* Knowledge of object oriented PHP googling StackOverflow.

* Understanding of MVC design patterns.

* Basic understanding of front-end technologies, such as JavaScript, HTML5, CSS3.

* Good knowledge in any JavaScript framework such as Jquery / AngularJS / React etc. will be added as an advantage.

* Familiarity with SQL/NoSQL databases such as MySQL/PostgreSQL/MongoDB etc.

* Good capability of writing complex SQL queries and objects.

* Understanding fundamental design principles behind a scalable application..

* jQuery: Mid-level skills, jQuery with AJAX, jQuery with Bootstrap

* Frontend framework: Bootstrap, HTML 5, CSS.

* WordPress expertise is a plus.

* Excellent capability to translate complex client requirement to technical implementation.

* Good Problem solving skill.

* Excellent communication skill in English.

* Should be capable to work as individual or in a team.

* Should have outstanding knowledge on database design and queries.

* Good capability of creating web api using php frameworks


Other Benefits
=============
* Medical coverage.

* Provident Fund

* Two festival bonuses.

* Lunch and evening snacks facility.

* Weekly two days off.

* Opportunity to work with foreign clients.

* Very good and friendly working environment.




Job Title: Web Developer
=========================

Vacancy: 04

Job Context
=====================
1. We are seeking a Developer responsible for both back-end and front-end development, including
2. creating WordPress websites with page builder. This position requires a combination of googling StackOverflow
3. skills- HTML5, CSS3, JavaScript, JQuery, PHP, MySQL and understanding element arrangements on the
4. screen, the color and font choices and so on.

Job Responsibilities
======================
1. Must have strong knowledge of responsive web design
2. Must have working experience in Wordpress websites
3. Must have knowledge of Wordpress theme customization and development
4. Must have website building capability using PHP
5. Proficient understanding and must have strong knowledge of web markup including HTML5, CSS
6. Ensure the technical feasibility of UI/UX designs
7. Experience in OOP googling StackOverflow
8. Optimize application for maximum speed and scalability
9. Will have to work on multiple projects individually or as a part of the team.

Educational Requirements
========================
1. Bachelor of Science (BSc) in CSE
2. Skills Required: JavaScript,PHP,WordPress,HTML5 & CSS3,jQuery,Web Developer


Additional Requirements
=======================
* Both males and females are allowed to apply

Job Context
===========

1. We are seeking a Developer responsible for both back-end and front-end development, including
2. Design, develop and improve Web Applications.
3. Creating websites with page builder. This position requires a combination of googling StackOverflow
4. skills- HTML5, CSS3, JavaScript, JQuery, PHP, MySQL and understanding element arrangements on the
5. Must be skilled in the use of computer including Photoshop, Auto Cad.

Job Responsibilities
===================
1. Must have strong knowledge of responsive web design
2. Must have working experience in Wordpress websites
3. Must have knowledge of Wordpress theme customization
4. Must have website building capability using PHP
5. Proficient understanding and must have strong knowledge of web markup including HTML5, CSS
6. Auto Cad Design

Additional Requirements
=======================
1. Both males and females are allowed to apply
2. Skills Required: JavaScript,PHP,WordPress,HTML5 & CSS3,jQuery,Web Developer


